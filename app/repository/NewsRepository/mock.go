package NewsRepository

import (
	"example/app/models"
	"gitlab.com/devpro_studio/Paranoia/paranoia/repository"
)

type Mock struct {
	repository.Mock
	Data []*models.News
}

func (t *Mock) GetActual(_ int64) []*models.News {
	return t.Data
}
