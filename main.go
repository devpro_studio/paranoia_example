package main

import (
	"example/app/controllers"
	"example/app/repository/NewsRepository"
	"gitlab.com/devpro_studio/Paranoia/paranoia"
	"gitlab.com/devpro_studio/Paranoia/paranoia/telemetry"
	"gitlab.com/devpro_studio/Paranoia/pkg/cache/memory"
	"gitlab.com/devpro_studio/Paranoia/pkg/database/postgres"
	"gitlab.com/devpro_studio/Paranoia/pkg/logger/std-log"
	"gitlab.com/devpro_studio/Paranoia/pkg/server/http"
)

func main() {
	s := paranoia.
		New(
			"example paranoia app",
			"example_cfg.yaml",
		)

	s.SetMetrics(telemetry.NewMetricStd("std"))
	s.SetTrace(telemetry.NewTraceStd("std"))

	s.PushPkg(std_log.New("log")).
		PushPkg(memory.New("memory")).
		PushPkg(postgres.New("primary")).
		PushPkg(http.New("web")).
		PushModule(&NewsRepository.Repository{}).
		PushModule(&controllers.NewsController{})

	err := s.Init()

	if err != nil {
		panic(err)
		return
	}

	defer s.Stop()

	select {}
}
