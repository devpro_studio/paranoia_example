package controllers

import (
	"bytes"
	"context"
	"example/app/models"
	"example/app/repository/NewsRepository"
	"gitlab.com/devpro_studio/Paranoia/pkg/server/http"
	"testing"
)

func TestNewsController_List(t1 *testing.T) {
	tests := []struct {
		name string
		ctx  http.ICtx
		data []*models.News
		want []byte
	}{
		{
			"base empty list test",
			http.HttpCtxPool.Get().(http.ICtx),
			[]*models.News{},
			[]byte("[]"),
		},
		{
			"base list test one element",
			http.HttpCtxPool.Get().(http.ICtx),
			[]*models.News{
				{1, "news 1", "desc"},
			},
			[]byte("[{\"id\":1,\"title\":\"news 1\",\"description\":\"desc\"}]"),
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &NewsController{
				newsRepository: &NewsRepository.Mock{Data: tt.data},
			}

			t.List(context.Background(), tt.ctx)

			if !bytes.Equal(tt.want, tt.ctx.GetResponse().GetBody()) {
				t1.Error("want ", string(tt.want), " got ", string(tt.ctx.GetResponse().GetBody()))
			}
		})
	}
}
