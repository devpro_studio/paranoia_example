package NewsRepository

import (
	"context"
	"example/app/models"
	"gitlab.com/devpro_studio/Paranoia/paranoia/interfaces"
	"gitlab.com/devpro_studio/Paranoia/paranoia/repository"
	"gitlab.com/devpro_studio/Paranoia/pkg/database/postgres"
)

type Repository struct {
	repository.Mock
	db *postgres.Postgres
}

func (t *Repository) Init(app interfaces.IEngine, _ map[string]interface{}) error {
	t.db = app.GetPkg(interfaces.PkgDatabase, "primary").(*postgres.Postgres)
	return nil
}

func (t *Repository) Name() string {
	return "Repository"
}

func (t *Repository) GetActual(count int64) []*models.News {
	var res []*models.News

	query, err := t.db.Query(context.TODO(), "select id, title, description from news limit ?", count)
	if err != nil {
		return res
	}

	for query.Next() {
		var news models.News

		if err = query.Scan(&news.ID, &news.Title, &news.Description); err != nil {
			return res
		}

		res = append(res, &news)
	}

	return res
}
