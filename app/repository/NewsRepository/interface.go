package NewsRepository

import (
	"example/app/models"
)

type Interface interface {
	GetActual(count int64) []*models.News
}
