package controllers

import (
	"context"
	"encoding/json"
	"example/app/repository/NewsRepository"
	"gitlab.com/devpro_studio/Paranoia/paranoia/controller"
	"gitlab.com/devpro_studio/Paranoia/paranoia/interfaces"
	"gitlab.com/devpro_studio/Paranoia/pkg/server/http"
)

type NewsController struct {
	controller.Mock
	app            interfaces.IEngine
	newsRepository NewsRepository.Interface
}

func (t *NewsController) Init(app interfaces.IEngine, _ map[string]interface{}) error {
	t.app = app
	t.newsRepository = app.GetModule(interfaces.ModuleRepository, "Repository").(NewsRepository.Interface)

	app.GetPkg(interfaces.PkgServer, "web").(*http.Http).PushRoute("GET", "/", t.List, nil)
	return nil
}

func (t *NewsController) Name() string {
	return "NewsController"
}

func (t *NewsController) List(c context.Context, ctx http.ICtx) {
	news := t.newsRepository.GetActual(10)

	r, _ := json.Marshal(news)
	ctx.GetResponse().SetBody(r)
}
